//
//  RestClient.swift
//  RestClient
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import Foundation
import CoreData
import Combine

//MARK: - URLSessionProtocol

public protocol URLSessionProtocol
{
    typealias APIResponse = URLSession.DataTaskPublisher.Output
    func performRequest(for url: URL) -> AnyPublisher<APIResponse, URLError>
}

extension URLSession: URLSessionProtocol
{
    public func performRequest(for url: URL) -> AnyPublisher<APIResponse, URLError>
    {
        return self.dataTaskPublisher(for: url).eraseToAnyPublisher()
    }
}

//MARK: - RestClient

public class RestClient
{
    var url : URL
    var context : NSManagedObjectContext
    var session : URLSessionProtocol
    
    public init(endpoint: String, context: NSManagedObjectContext, session: URLSessionProtocol)
    {
        guard let url = URL(string: endpoint) else
        {
            fatalError("Invalid URL provided.")
        }
        
        self.url = url
        self.context = context
        self.session = session
    }
    
    public func data<T: Decodable>() -> AnyPublisher<[T], Error>
    {
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.init(rawValue: "managedObjectContext")!] = self.context
        
        return self.session.performRequest(for: url)
                .tryMap { data, response in
                    guard let httpResponse = response as? HTTPURLResponse else {
                        throw NetworkError.network
                    }
                    
                    guard httpResponse.statusCode == 200 else {
                        throw NetworkError.server
                    }
                    
                    return data
                }
                .decode(type: [T].self, decoder: decoder)
                .map { items in
                    items
                }
                .mapError { error in
                    if let decodingError = error as? DecodingError {
                        return NetworkError.parsing
                    } else {
                        return error
                    }
                }
                .eraseToAnyPublisher()
    }
}
