//
//  RestError.swift
//  RestClient
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

public enum NetworkError
{
    case url
    case network
    case parsing
    case notFound
    case server
}

extension NetworkError : LocalizedError
{
    public var errorDescription: String?
    {
        switch self
        {
            case .url: return "URL not defined"
            case .network: return "Network error"
            case .parsing: return "Parsing error"
            case .notFound: return "URL Not Found"
            case .server: return "Server Error"
        }
    }

    public var errorCode: Int?
    {
        switch self
        {
            case .notFound: return 404
            case .server: return 500
            case .url, .network, .parsing: return nil
        }
    }
}

