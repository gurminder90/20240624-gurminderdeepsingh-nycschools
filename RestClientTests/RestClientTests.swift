//
//  RestClientTests.swift
//  RestClientTests
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import CoreData
import Combine
@testable import RestClient
import XCTest

class RestClientTests: XCTestCase
{
    private var cancellables = Set<AnyCancellable>()

    struct mockDecodableModel: Decodable
    {
        var id: String?
        var name: String?

        private enum Keys: String, CodingKey
        {
            case id
            case name
        }

        init(from decoder: Decoder) throws
        {
            let values = try decoder.container(keyedBy: Keys.self)
            self.id = try values.decodeIfPresent(String.self, forKey: .id)
            self.name = try values.decodeIfPresent(String.self, forKey: .name)
        }
    }

    class mockURLSession: URLSessionProtocol
    {
        var response: [[String: String]] = []
        
        func performRequest(for url: URL) -> AnyPublisher<APIResponse, URLError>
        {
            guard let data = try? JSONEncoder().encode(self.response),
                    let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: "1.1", headerFields: nil) else
            {
                return Fail(error: URLError(URLError.Code.badServerResponse))
                    .eraseToAnyPublisher()
            }
            
            return Just((data: data, response: response))
                .setFailureType(to: URLError.self)
                .eraseToAnyPublisher()
        }
    }

    override func setUp()
    {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRequest()
    {
        let endpoint = "https://testurl.com"
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        let session = mockURLSession()
        session.response = [
            [
                "id": "1",
                "name": "John Doe",
            ],
            [
                "id": "2",
                "name": "Jane Doe",
            ],
        ]

        let client = RestClient(endpoint: endpoint, context: context, session: session)
        client.data().sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                break
            case .failure:
                break
            }
        }, receiveValue: { (items: [mockDecodableModel]) in
            
            XCTAssertEqual(2, items.count)

            XCTAssertNotNil(items.first)
            XCTAssertEqual(items.first?.id, "1")
            XCTAssertEqual(items.first?.name, "John Doe")
            XCTAssertNotNil(items.last)
            XCTAssertEqual(items.last?.id, "2")
            XCTAssertEqual(items.last?.name, "Jane Doe")
        }).store(in: &cancellables)
    }
}
