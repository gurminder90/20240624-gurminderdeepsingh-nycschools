//
//  ModelsTests.swift
//  ModelsTests
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import XCTest
import CoreData
@testable import Models

class StoreTests: XCTestCase
{
    lazy var modelStore : IModelStore = {
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        
        let container = NSPersistentContainer(name: "StoreTests", managedObjectModel: ModelStore.model)
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            
            precondition( description.type == NSInMemoryStoreType )
            if let error = error
            {
                fatalError("Creating in memory store coordinator failed \(error)")
            }
        }
        
        return ModelStore(container: container)
    }()
    
    override func setUp()
    {
  
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCRUDSchool() throws
    {
        let newItem = School(insertInto: self.modelStore.container.viewContext)
        self.modelStore.save()
        
        let request = NSFetchRequest<School>(entityName: "School")
        XCTAssertEqual(1, try self.modelStore.container.viewContext.fetch(request).count)
        
        newItem.name = "Test School"
        self.modelStore.save()
        
        let existingSchool = try self.modelStore.container.viewContext.fetch(request).first!
        XCTAssertEqual(existingSchool.name, newItem.name)
        XCTAssertEqual(existingSchool.name, "Test School")
        
        self.modelStore.container.viewContext.delete(existingSchool)
        XCTAssertEqual(0, try self.modelStore.container.viewContext.fetch(request).count)
    }
    
    func testCRUDBorough() throws
    {
        let newItem = Borough(insertInto: self.modelStore.container.viewContext)
        self.modelStore.save()
        
        let request = NSFetchRequest<Borough>(entityName: "Borough")
        XCTAssertEqual(1, try self.modelStore.container.viewContext.fetch(request).count)
        
        newItem.name = "Bronx"
        self.modelStore.save()
        
        let existingItem = try self.modelStore.container.viewContext.fetch(request).first!
        XCTAssertEqual(existingItem.name, newItem.name)
        XCTAssertEqual(existingItem.name, "Bronx")
        
        self.modelStore.container.viewContext.delete(existingItem)
        XCTAssertEqual(0, try self.modelStore.container.viewContext.fetch(request).count)
    }

    func testCRUDAddress() throws
    {
        let newItem = Address(insertInto: self.modelStore.container.viewContext)
        self.modelStore.save()

        let request = NSFetchRequest<Address>(entityName: "Address")

        XCTAssertEqual(1, try self.modelStore.container.viewContext.fetch(request).count)

        newItem.street1 = "1 address way"
        self.modelStore.save()

        let existingItem = try self.modelStore.container.viewContext.fetch(request).first!
        XCTAssertEqual(existingItem.street1, newItem.street1)
        XCTAssertEqual(existingItem.street1, "1 address way")

        self.modelStore.container.viewContext.delete(existingItem)
        XCTAssertEqual(0, try self.modelStore.container.viewContext.fetch(request).count)
    }
}
