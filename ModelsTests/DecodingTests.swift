//
//  ModelsTests.swift
//  ModelsTests
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import XCTest
import CoreData
@testable import Models

class DecodingTests: XCTestCase
{
    lazy var modelStore : IModelStore = {
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        
        let container = NSPersistentContainer(name: "StoreTests", managedObjectModel: ModelStore.model)
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            
            precondition( description.type == NSInMemoryStoreType )
            if let error = error
            {
                fatalError("Creating in memory store coordinator failed \(error)")
            }
        }
        
        return ModelStore(container: container)
    }()
    
    var schoolInfo : [String : String]
    {
        return  [
            "dbn" : "aaaa",
            "school_name" : "Test High School",
            "overview_paragraph" : "Test",
            "website" : "https://www.testhighschool.com",
            "school_email" : "admin@testhighschool.com",
            "phone_number" : "123-456-7890",
            "fax_number" : "098-765-4321",
            "total_students" : "123"
        ]
    }
    
    var boroughInfo : [String : String]
    {
        return [
            "boro" : "X",
            "borough" : "Bronx"
        ]
    }
    
    var addressInfo : [String : String]
    {
        return [
            "primary_address_line_1" : "100 West Mosholu Parkway South",
            "city" : "Bronx",
            "zip" : "10468",
            "latitude" : "40.87996",
            "longitude" : "-73.889"
        ]
    }
    
    
    override func setUp()
    {
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func compareSchool(_ school: School,_ info: [String: String])
    {
        XCTAssertEqual(school.dbn, info["dbn"])
        XCTAssertEqual(school.name, info["school_name"])
        XCTAssertEqual(school.overview, info["overview_paragraph"])
        XCTAssertEqual(school.website, info["website"])
        XCTAssertEqual(school.email, info["school_email"])
        XCTAssertEqual(school.phone_number, info["phone_number"])
        XCTAssertEqual(school.fax_number, info["fax_number"])
        
        XCTAssertNotNil(school.total_students)
        XCTAssertEqual(String(school.total_students?.intValue ?? 0), info["total_students"])
    }

    func testDecodeJSONToSchool() throws
    {
        let info = self.schoolInfo
        
        let jsonData = try JSONEncoder().encode(info)
                
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext!] = self.modelStore.container.viewContext
        
        let school = try decoder.decode(School.self, from: jsonData)
        self.compareSchool(school, info)
    }
    
    func compareBorough(_ borough: Borough, _ info : [String: String])
    {
        XCTAssertEqual(borough.code, info["boro"])
        XCTAssertEqual(borough.name, info["borough"])
    }
    
    func testDecodeJSONToBorough() throws
    {
        let info = self.boroughInfo
        
        let jsonData = try JSONEncoder().encode(info)
                
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext!] = self.modelStore.container.viewContext
        
        let borough = try decoder.decode(Borough.self, from: jsonData)
        self.compareBorough(borough, info)
    }
    
    func compareAddress(_ address: Address, _ info : [String: String])
    {
        XCTAssertEqual(address.street1, info["primary_address_line_1"])
        XCTAssertEqual(address.city, info["city"])
        XCTAssertEqual(address.zip, info["zip"])
        
        XCTAssertNotNil(address.latitude)
        XCTAssertNotNil(address.longitude)
        
        XCTAssertEqual(String(address.latitude?.floatValue ?? 0), info["latitude"])
        XCTAssertEqual(String(address.longitude?.floatValue ?? 0), info["longitude"])
    }
    
    func testDecodeJSONToAddress() throws
    {
        let info = self.addressInfo
        
        let jsonData = try JSONEncoder().encode(info)
                
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext!] = self.modelStore.container.viewContext
        
        let address = try decoder.decode(Address.self, from: jsonData)
        self.compareAddress(address, info)
    }
    
    func testDecodeFullJSON() throws
    {
        var info = self.schoolInfo
        info.merge(self.boroughInfo){(current, _) in current}
        info.merge(self.addressInfo){(current, _) in current}
        
        let jsonData = try JSONEncoder().encode(info)
                
        let decoder = JSONDecoder()
        decoder.userInfo[CodingUserInfoKey.managedObjectContext!] = self.modelStore.container.viewContext
        
        let school = try decoder.decode(School.self, from: jsonData)
        self.compareSchool(school, info)

        XCTAssertNotNil(school.borough)
        self.compareBorough(school.borough!, info)
        
        XCTAssertNotNil(school.address)
        self.compareAddress(school.address!, info)
    }
}
