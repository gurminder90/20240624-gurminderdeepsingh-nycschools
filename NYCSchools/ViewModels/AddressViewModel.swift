//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit
import Models
import CoreLocation

class AddressViewModel
{
    private var address : Address
    init(model : Address)
    {
        self.address = model
    }
    
    var street : String {
        return self.address.street1 ?? "-"
    }
    
    var city : String {
        return self.address.city ?? "-"
    }
    
    var zip : String {
        return self.address.zip ?? "-"
    }
    
    var location: CLLocationCoordinate2D?
    {
        if let latitude = self.address.latitude, let longitude = self.address.longitude
        {
            return CLLocationCoordinate2D(latitude: latitude.doubleValue, longitude: longitude.doubleValue)
        }
        return nil
    }
}
