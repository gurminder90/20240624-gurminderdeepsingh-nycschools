//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit
import Models

class SchoolViewModel
{
    private static let numberFormatter = NumberFormatter()
    private var school : School
    init(model : School)
    {
        self.school = model
    }
    
    var name : String {
        return self.school.name ?? "-"
    }
    
    var phone : String {
        return self.school.phone_number ?? "-"
    }
    
    var fax : String {
        return self.school.fax_number ?? "-"
    }
    
    var email : String {
        return self.school.email ?? "-"
    }
    
    var website : String {
        return self.school.website ?? "-"
    }
    
    var totalStudents : String {
        
        if let students = self.school.total_students?.intValue
        {
            return String(students)
        }
        
        return "-"
    }
    
    var attendence : String {
        
        if self.school.attendance == nil
        {
            return "-"
        }
        
        Self.numberFormatter.numberStyle = .percent
        Self.numberFormatter.maximumFractionDigits = 0
        Self.numberFormatter.multiplier = 100
        
        return Self.numberFormatter.string(from: self.school.attendance!)!
    }
    
    var graduation : String {
           
           if self.school.graduation == nil
           {
               return "-"
           }
           
           Self.numberFormatter.numberStyle = .percent
           Self.numberFormatter.maximumFractionDigits = 2
           Self.numberFormatter.multiplier = 100
           
           return Self.numberFormatter.string(from: self.school.graduation!)!
       }
    
    var borough : BoroughViewModel?
    {
        return self.school.borough == nil ? nil : BoroughViewModel(model: self.school.borough!)
    }
    
    var sat : SatViewModel?
    {
        return self.school.sat == nil ? nil : SatViewModel(model: self.school.sat!)
    }
    
    var address : AddressViewModel?
    {
        return self.school.address == nil ? nil : AddressViewModel(model: self.school.address!)
    }
}
