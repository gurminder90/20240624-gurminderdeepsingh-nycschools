//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit
import Models

class BoroughViewModel
{
    private var borough : Borough
    init(model : Borough)
    {
        self.borough = model
    }
    
    var name : String {
        return self.borough.name ?? "-"
    }
    
    var code : String {
        return self.borough.code ?? "-"
    }
}
