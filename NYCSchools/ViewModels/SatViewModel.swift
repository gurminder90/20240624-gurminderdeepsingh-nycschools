//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import Models

class SatViewModel
{
    private static let numberFormatter = NumberFormatter()
    private var sat : SAT
    init(model : SAT)
    {
        self.sat = model
    }

    var totalStudents : String {
        
        if let score = self.sat.total_students?.intValue
        {
            return String(score)
        }
        
        return "-"
    }
    
    var mathScore : String {
        
        if let score = self.sat.math_score?.intValue
        {
            return String(score)
        }
        
        return "-"
    }
    
    var readingScore : String {
        
        if let score = self.sat.reading_score?.intValue
        {
            return String(score)
        }
        
        return "-"
    }
    
    var writingScore : String {
        
        if let score = self.sat.writing_score?.intValue
        {
            return String(score)
        }
        
        return "-"
    }
}
