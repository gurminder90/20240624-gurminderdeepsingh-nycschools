//
//  AppDelegate.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import CoreData
import Models
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?

    var coordinator: AppCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        UITableView.appearance().separatorColor = UIColor.black.withAlphaComponent(0.10)

        let rootNC = UINavigationController()
        self.coordinator = AppCoordinator(navigationController: rootNC)
        self.coordinator?.wait()
        self.window = UIWindow()
        self.window?.tintColor = UIColor(named: "AppColor")
        self.window?.rootViewController = rootNC
        self.window?.makeKeyAndVisible()

        Task
        { [weak self] in
            do
            {
                try await self?.modelStore.initializeData()
                self?.coordinator?.start()
            }
            catch
            {
                self?.coordinator?.failed()
            }
        }

        return true
    }

    // MARK: - Core Data stack

    lazy var modelStore: ModelStore = {
        /*
          The persistent container for the application. This implementation
          creates and returns a container, having loaded the store for the
          application to it. This property is optional since there are legitimate
          error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "NYCSchools", managedObjectModel: ModelStore.model)
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error as NSError?
            {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })

        return ModelStore(container: container)
    }()
}
