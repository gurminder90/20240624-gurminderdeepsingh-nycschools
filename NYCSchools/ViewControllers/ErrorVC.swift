//
//  ErrorVC.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class ErrorVC: UIViewController, FromStoryboard
{
    var coordinator : (CanShowShoolInfo & CanShowBoroughPicker)?

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
