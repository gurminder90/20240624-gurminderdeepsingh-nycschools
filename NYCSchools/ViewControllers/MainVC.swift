//
//  ViewController.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit
import MapKit

class MainVC: UIViewController, FromStoryboard, UITableViewDelegate, MKMapViewDelegate
{
    var coordinator : (CanShowShoolInfo & CanShowBoroughPicker)?
    
    var tableDataSource : SchoolsTableViewDataSource?
    var mapDataSource : SchoolsMapViewDataSource?
    
    var showingMap : Bool = true
    {
        didSet
        {
            self.btnSwitchView.setTitle(self.showingMap ? "List View" : "Map View", for: .normal)
            self.viwMap.isHidden = !self.showingMap
            self.btnCenter.isHidden = !self.showingMap
            self.viwTable.isHidden = self.showingMap
        }
    }
    
    @IBOutlet weak var viwMap: MKMapView!
    @IBOutlet weak var viwTable: UITableView!
    
    @IBOutlet weak var btnSwitchView: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnCenter: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viwTable.delegate = self
        self.viwTable.contentInset.bottom = 100.0
        
        self.tableDataSource = SchoolsTableViewDataSource(tableView: self.viwTable)
        self.tableDataSource?.loadData()
        
        self.viwMap.mapType = .mutedStandard
        self.viwMap.delegate = self
        self.mapDataSource = SchoolsMapViewDataSource(mapView: self.viwMap)
        self.mapDataSource?.loadData()
        
        self.showingMap = true
        
        self.customizeFloating(button: self.btnSwitchView)
        self.customizeFloating(button: self.btnFilter)
        self.customizeFloating(button: self.btnCenter)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.actionCenter()
    }
    
    func applyFilter(predicate : NSPredicate?)
    {
        self.tableDataSource?.applyFilter(predicate: predicate)
        self.mapDataSource?.applyFilter(predicate: predicate)
    }
    
    //MARK: - UI Customizations
    
    private func customizeFloating(button: UIButton)
    {
        button.layer.cornerRadius = button.frame.height / 2.0
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 1)
        button.layer.shadowRadius = 5.0
        button.layer.shadowOpacity = 0.50
        button.backgroundColor = UIColor.white
    }
    
    private func customizeBottom(button: UIButton)
    {
        button.layer.cornerRadius = button.frame.height / 2.0
        button.layer.borderColor = UIColor.black.withAlphaComponent(0.10).cgColor
        button.layer.borderWidth = 1.0
    }
    
    //MARK: - Button Actions
    @IBAction func btnActionSwitchView()
    {
        self.showingMap = !self.showingMap
    }
    
    @IBAction func actionReset()
    {
        self.applyFilter(predicate: nil)
    }
    
    @IBAction func actionFilter()
    {
        self.coordinator?.showBoroughPicker(completion: { (borough) in
            self.applyFilter(predicate: NSPredicate(format: "borough.name = %@", borough.name))
        })
    }
    
    @IBAction func actionCenter()
    {
        let newYorkCenter = CLLocationCoordinate2DMake(40.7128, -74.0060)
        let region = MKCoordinateRegion(center: newYorkCenter, span: MKCoordinateSpan(latitudeDelta: 0.8, longitudeDelta: 0.8))
        self.viwMap.regionThatFits(region)
        self.viwMap.setRegion(region, animated: true)
    }
    
    //MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        if let vm = self.tableDataSource?.list[indexPath.row]
        {
            self.coordinator?.showSchoolInfo(school: vm)
        }
    }
    
    //MARK: Map View Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        return self.mapDataSource?.view(for: annotation, in: mapView)
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        if let vm = (view.annotation as? MapAnnotation)?.school
        {
            self.coordinator?.showSchoolInfo(school: vm)
        }
    }
}

