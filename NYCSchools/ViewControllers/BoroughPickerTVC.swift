//
//  SchoolDetailTVC.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

protocol BoroughPickerDelegate : AnyObject
{
    func boroughPickerDidSelect(borough: BoroughViewModel)
}

class BoroughPickerTVC: UITableViewController, FromStoryboard
{
    var coordinator : BoroughPickerCoordinator?
    var dataSource : BoroughsTableViewDataSource?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.dataSource = BoroughsTableViewDataSource(tableView: self.tableView)
        self.dataSource?.loadData()
        
        self.navigationItem.title = "Select Borough"
    }
    
    // MARK: - Table view Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        if let vm = self.dataSource?.list[indexPath.row]
        {
            self.coordinator?.boroughPickerDidSelect(borough: vm)
        }
        self.dismiss(animated: true)
    }
}
