//
//  SchoolDetailTVC.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import MapKit
import UIKit

class SchoolDetailTVC: UITableViewController, FromStoryboard, MKMapViewDelegate
{
    @IBOutlet weak var viwMap: MKMapView!

    var coordinator: SchoolDetailCoordinator?
    var school: SchoolViewModel?

    var dataSource: SchoolDetailTableViewDataSource?

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.viwMap.mapType = .mutedStandard
        self.viwMap.isUserInteractionEnabled = false

        if let school = self.school, let location = school.address?.location
        {
            self.viwMap.delegate = self
            let annotation = MapAnnotation(school: school)
            self.viwMap.addAnnotation(annotation)

            let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            self.viwMap.regionThatFits(region)
            self.viwMap.setRegion(region, animated: true)
        }

        self.dataSource = SchoolDetailTableViewDataSource(tableView: self.tableView)
        self.dataSource?.school = self.school
        self.dataSource?.loadData()

        self.navigationItem.title = "School Details"
    }

    // MARK: table view
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)

        if let rowValue = self.dataSource?.list[indexPath.section].rows[indexPath.row].value,
           let url = URL(string: "http://\(rowValue)"),
           UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url)
        }
    }

    // MARK: Map View Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        let identifier = "SchoolAnnotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil
        {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.rightCalloutAccessoryView = nil
            annotationView?.canShowCallout = false
            annotationView?.image = UIImage(named: "Pin")
        }
        else
        {
            annotationView?.annotation = annotation
        }

        return annotationView
    }
}
