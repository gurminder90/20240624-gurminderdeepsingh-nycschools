//
//  BoroughCell.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class BoroughCell: UITableViewCell
{
    @IBOutlet private weak var lblName: UILabel!
    
    var title: String?
    {
        didSet
        {
            self.lblName.text = title ?? "Unknown"
        }
    }
}
