//
//  SchoolCell.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell
{
    @IBOutlet private weak var lblName: UILabel!
    @IBOutlet private weak var lblCity: UILabel!
    @IBOutlet private weak var lblBorough: UILabel!

    func set(name: String, city: String?, borough: String?)
    {
        self.lblName.text = name
        self.lblCity.text = city ?? "-"
        self.lblBorough.text = borough ?? "-"
    }
    
}
