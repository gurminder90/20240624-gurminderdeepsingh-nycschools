//
//  InfoCell.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell
{
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblValue: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func set(title: String, value: String?)
    {
        self.lblTitle.text = title
        self.lblValue.text = value ?? "-"
    }
}
