//
//  NavigationCoordinator.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class AppCoordinator: INavigationCoordinator, CanShowShoolInfo, CanShowBoroughPicker
{
    weak var navigationController: UINavigationController?

    required init(navigationController: UINavigationController?)
    {
        self.navigationController = navigationController
    }

    func wait()
    {
        self.navigationController?.navigationBar.isHidden = true
        let storyboard = UIStoryboard(name: "LaunchScreen", bundle: Bundle.main)
        let launchVC = storyboard.instantiateViewController(withIdentifier: "LaunchVC")
        self.navigationController?.viewControllers = [launchVC]
    }

    func start()
    {
        self.navigationController?.navigationBar.isHidden = false
        let mainVC = MainVC.instantiate()
        mainVC.coordinator = self
        self.navigationController?.viewControllers = [mainVC]
    }

    func failed()
    {
        self.navigationController?.navigationBar.isHidden = true
        let errorVC = ErrorVC.instantiate()
        errorVC.coordinator = self
        self.navigationController?.viewControllers = [errorVC]
    }

    func showSchoolInfo(school: SchoolViewModel)
    {
        let schoolDetailCoordinator = SchoolDetailCoordinator(navigationController: self.navigationController)
        schoolDetailCoordinator.school = school
        schoolDetailCoordinator.start()
    }

    func showBoroughPicker(completion: @escaping (BoroughViewModel) -> Void)
    {
        let pickerCoordinator = BoroughPickerCoordinator(navigationController: self.navigationController)
        pickerCoordinator.completion = completion
        pickerCoordinator.start()
    }
}
