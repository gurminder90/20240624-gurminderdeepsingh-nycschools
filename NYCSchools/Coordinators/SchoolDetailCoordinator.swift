//
//  NavigationCoordinator.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class SchoolDetailCoordinator: INavigationCoordinator
{
    weak var parentCoordinator: AppCoordinator?
    
    var school: SchoolViewModel?
    
    var navigationController: UINavigationController?
        
    required init(navigationController: UINavigationController?)
    {
        self.navigationController = navigationController
    }
    
    func start()
    {
        let detailVC = SchoolDetailTVC.instantiate()
        detailVC.coordinator = self
        detailVC.school = self.school
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

protocol CanShowShoolInfo
{
    func showSchoolInfo(school: SchoolViewModel)
}
