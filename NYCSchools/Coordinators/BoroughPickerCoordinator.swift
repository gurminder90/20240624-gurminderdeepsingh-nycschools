//
//  NavigationCoordinator.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class BoroughPickerCoordinator: INavigationCoordinator, BoroughPickerDelegate
{
    var completion : ((BoroughViewModel) -> Void)?
    
    weak var navigationController: UINavigationController?
        
    required init(navigationController: UINavigationController?)
    {
        self.navigationController = navigationController
    }
    
    func start()
    {
        let picker = BoroughPickerTVC()
        picker.coordinator = self
        
        let nav = UINavigationController(rootViewController: picker)
        self.navigationController?.present(nav, animated: true)
    }
    
    //MARK: - Borough Picker Delegate
    func boroughPickerDidSelect(borough: BoroughViewModel)
    {
        self.completion?(borough)
        self.completion = nil
    }
}

protocol CanShowBoroughPicker
{
    func showBoroughPicker(completion: @escaping (BoroughViewModel) -> Void)
}
