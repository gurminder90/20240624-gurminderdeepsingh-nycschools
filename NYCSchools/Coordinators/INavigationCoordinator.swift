//
//  INavigationCoordinator.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

protocol INavigationCoordinator : AnyObject
{
    var navigationController : UINavigationController? { get set }
    
    init(navigationController: UINavigationController?)
    func start()
}
