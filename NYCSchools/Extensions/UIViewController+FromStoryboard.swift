//
//  UIViewController+FromStoryboard.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

protocol FromStoryboard
{
    static func instantiate() -> Self
}

extension FromStoryboard where Self : UIViewController
{
    static func instantiate() -> Self
    {
        guard let className = NSStringFromClass(Self.self).split(separator: ".").last else
        {
            fatalError("Unable to get class name for controller")
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: String(className)) as! Self
    }
}
