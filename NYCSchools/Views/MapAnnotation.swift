//
//  MapAnnotation.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit
import MapKit

class MapAnnotation: NSObject, MKAnnotation
{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    
    var school : SchoolViewModel
    
    init(school : SchoolViewModel)
    {
        self.school = school
        self.title = self.school.name
        
        if let location = self.school.address?.location
        {
            self.coordinate = location
        }
        else
        {
            self.coordinate = CLLocationCoordinate2DMake(0, 0)
        }
        super.init()
    }
}
