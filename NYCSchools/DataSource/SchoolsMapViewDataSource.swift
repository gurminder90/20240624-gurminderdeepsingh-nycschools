//
//  TableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import MapKit

class SchoolsMapViewDataSource: SchoolsDataSource, IMapViewDataSource
{
    private let reuseIdentifier = "SchoolAnnotation"

    internal var mapView: MKMapView

    required init(mapView: MKMapView)
    {
        self.mapView = mapView
    }

    // MARK: - DataSource
    override func reloadData()
    {
        self.updateMap(schools: self.list)
    }

    private func updateMap(schools: [SchoolViewModel])
    {
        self.mapView.removeAnnotations(self.mapView.annotations)

        var annotations = [MapAnnotation]()
        for school in schools
        {
            if school.address?.location != nil
            {
                annotations.append(MapAnnotation(school: school))
            }
        }

        self.mapView.addAnnotations(annotations)
    }

    func view(for annotation: MKAnnotation, in mapView: MKMapView) -> MKAnnotationView?
    {
        let identifier = "SchoolAnnotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil
        {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView?.canShowCallout = true
            annotationView?.image = UIImage(named: "Pin")
            annotationView?.tintColor = UIColor(named: "AppColor")
        }
        else
        {
            annotationView?.annotation = annotation
        }

        return annotationView
    }
}
