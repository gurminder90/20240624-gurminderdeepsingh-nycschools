//
//  TableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class SchoolDetailTableViewDataSource: NSObject, IDataSource, ITableViewDataSource
{
    struct Section
    {
        struct Row
        {
            var title: String
            var value: String?
        }
        
        var title: String
        var rows: [Row] = []
    }
    
    private let reuseIdentifier = "InfoCell"
    
    internal var tableView: UITableView
    internal var predicate: NSPredicate?
    
    var school : SchoolViewModel?
    
    required init(tableView: UITableView)
    {
        self.tableView = tableView
        super.init()
        tableView.dataSource = self
        tableView.register(UINib(nibName: "InfoCell", bundle: Bundle.main), forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    //MARK: - DataSource
    var list: [Section] = []

    func loadData()
    {
        self.list.append(Section(title: "Information", rows: [
            Section.Row(title: "Name", value: self.school?.name),
            Section.Row(title: "Phone", value: self.school?.phone),
            Section.Row(title: "Fax", value: self.school?.fax),
            Section.Row(title: "Email", value: self.school?.email),
            Section.Row(title: "URL", value: self.school?.website),
        ]))
        
        self.list.append(Section(title: "Enrollment", rows: [
            Section.Row(title: "Students", value: self.school?.totalStudents),
            Section.Row(title: "Attendance", value: self.school?.attendence),
            Section.Row(title: "Graduation", value: self.school?.graduation)
        ]))
        
        self.list.append(Section(title: "SAT", rows: [
            Section.Row(title: "Students", value: self.school?.totalStudents),
            Section.Row(title: "Math", value: self.school?.sat?.mathScore),
            Section.Row(title: "Reading", value: self.school?.sat?.readingScore),
            Section.Row(title: "Writing", value: self.school?.sat?.writingScore)
        ]))
        
        self.list.append(Section(title: "Location", rows: [
            Section.Row(title: "Street", value: self.school?.address?.street),
            Section.Row(title: "City", value: self.school?.address?.city),
            Section.Row(title: "Zip", value: self.school?.address?.zip),
            Section.Row(title: "Borough", value: self.school?.borough?.name)
        ]))
    }
    
    func reloadData()
    {
        self.tableView.reloadData()
    }
    
    //MARK: - Table View Datasource
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.list[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! InfoCell
        
        let row = self.list[indexPath.section].rows[indexPath.row]
        cell.set(title: row.title, value: row.value)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return self.list[section].title
    }
}
