//
//  TableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class SchoolsTableViewDataSource: SchoolsDataSource, ITableViewDataSource
{
    private let reuseIdentifier = "SchoolCell"
    
    internal var tableView: UITableView
    
    required init(tableView: UITableView)
    {
        self.tableView = tableView
        super.init()
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "SchoolCell", bundle: Bundle.main), forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    override func reloadData()
    {
        self.tableView.reloadData()
    }
    
    //MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! SchoolCell
        
        let school = self.list[indexPath.row]
        cell.set(name: school.name, city: school.address?.city, borough: school.borough?.name)
        return cell
    }
}
