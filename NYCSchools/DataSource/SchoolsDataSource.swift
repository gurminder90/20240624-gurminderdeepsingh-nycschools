//
//  TableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class SchoolsDataSource: NSObject, IDataSource
{
    //MARK: - DataSource
    var list: [SchoolViewModel] = []
    var predicate : NSPredicate?

    func applyFilter(predicate: NSPredicate?)
    {
        self.predicate = predicate
        self.loadData()
    }
    
    func loadData()
    {
        let modelStore = (UIApplication.shared.delegate as! AppDelegate).modelStore
        let items = modelStore.schools()
        { [weak self] request in
            request.predicate = self?.predicate
            request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true), NSSortDescriptor(key: "address.city", ascending: true)]
            request.relationshipKeyPathsForPrefetching = ["address", "borough"]
        }
        
        self.list = items.map({ (school) -> SchoolViewModel in
            return SchoolViewModel(model: school)
        })
        self.reloadData()
    }
    
    func reloadData()
    {
        
    }
}
