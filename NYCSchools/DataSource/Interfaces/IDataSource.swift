//
//  DataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

protocol IDataSource: AnyObject
{
    associatedtype T
    
    var list : [T] { get set }
    var predicate : NSPredicate? {get set}
    
    func loadData()
    func reloadData()
    
    func applyFilter(predicate: NSPredicate?)
}


extension IDataSource
{
    func applyFilter(predicate: NSPredicate?)
    {
        self.predicate = predicate
        self.loadData()
    }
}
