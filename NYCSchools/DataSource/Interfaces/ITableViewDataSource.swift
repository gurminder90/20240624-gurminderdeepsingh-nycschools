//
//  ITableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

protocol ITableViewDataSource: UITableViewDataSource
{
    var tableView : UITableView { get set }
    init(tableView: UITableView)
}
