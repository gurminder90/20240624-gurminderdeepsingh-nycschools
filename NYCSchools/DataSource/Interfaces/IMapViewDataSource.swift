//
//  ITableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import MapKit

protocol IMapViewDataSource
{
    var mapView : MKMapView { get set }
    init(mapView: MKMapView)
}
