//
//  TableViewDataSource.swift
//  NYCSchools
//
//  Created by Gurminder Deep Singh on 10/6/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import UIKit

class BoroughsTableViewDataSource: NSObject, IDataSource, ITableViewDataSource
{
    private let reuseIdentifier = "BoroughCell"
    
    internal var tableView: UITableView
    internal var predicate: NSPredicate?
    
    required init(tableView: UITableView)
    {
        self.tableView = tableView
        super.init()
        tableView.dataSource = self
        tableView.register(UINib(nibName: "BoroughCell", bundle: Bundle.main), forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    //MARK: - DataSource
    var list: [BoroughViewModel] = []

    func loadData()
    {
        let modelStore = (UIApplication.shared.delegate as! AppDelegate).modelStore
        let items = modelStore.boroughs()
        { request in
            request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        }
            
        self.list = items.map({ (borough) -> BoroughViewModel in
            return BoroughViewModel(model: borough)
        })
        
        self.reloadData()
    }
    
    func reloadData()
    {
        self.tableView.reloadData()
    }
    
    //MARK: - Table View Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath) as! BoroughCell
        cell.title = self.list[indexPath.row].name
        return cell
    }
}
