//
//  ModelStore.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import Combine
import CoreData
import Foundation
import RestClient

public protocol IModelStore
{
    static var model: NSManagedObjectModel { get }
    var container: NSPersistentContainer { get }

    init(container: NSPersistentContainer)

    func save()
    func save(context: NSManagedObjectContext)
}

public class ModelStore: IModelStore
{
    private var cancellables = Set<AnyCancellable>()

    static var bundle: Bundle = {
        Bundle(identifier: "com.gurminder.Models")!
    }()

    public static var model: NSManagedObjectModel
    {
        let url = Self.bundle.url(forResource: "Models", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: url)!
    }

    public var container: NSPersistentContainer
    {
        return self.persistentContainer
    }

    var persistentContainer: NSPersistentContainer

    public required init(container: NSPersistentContainer)
    {
        self.persistentContainer = container
    }

    public func save()
    {
        self.save(context: self.container.viewContext)
    }

    public func save(context: NSManagedObjectContext)
    {
        if context.hasChanges
        {
            do
            {
                try context.save()
            }
            catch
            {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    public func boroughs(updateRequest: ((NSFetchRequest<Borough>) -> Void)?) -> [Borough]
    {
        let request = NSFetchRequest<Borough>(entityName: NSStringFromClass(Borough.self))
        request.returnsDistinctResults = true
        updateRequest?(request)
        do
        {
            return try self.container.viewContext.fetch(request)
        }
        catch
        {
            return []
        }
    }

    public func schools(updateRequest: ((NSFetchRequest<School>) -> Void)?) -> [School]
    {
        let request = NSFetchRequest<School>(entityName: NSStringFromClass(School.self))
        updateRequest?(request)
        do
        {
            return try self.container.viewContext.fetch(request)
        }
        catch
        {
            return []
        }
    }

    public func initializeData() async throws
    {
        if self.schools(updateRequest: nil).count > 0
        {
            return
        }

        let context = self.container.newBackgroundContext()
        let info = ModelStore.bundle.infoDictionary!
        let endpoint = info["SchoolsAPI"] as? String
        let schoolsClient = RestClient(endpoint: endpoint ?? "", context: context, session: URLSession.shared)

        let _: [School] = try await withCheckedThrowingContinuation
        { continuation in
            schoolsClient.data().sink(receiveCompletion: { completion in
                switch completion
                {
                case .finished:
                    break
                case let .failure(error):
                    continuation.resume(throwing: error)
                }
            }, receiveValue: { [weak self] (schools: [School]) in
                self?.save(context: context)
                continuation.resume(returning: schools)
                self?.fetchScores()
            })
            .store(in: &cancellables)
        }

        // Any code that needs to be executed on the main thread should be awaited within a Task
        await MainActor.run
        {
            // Code that needs to run on the main thread
        }
    }

    func fetchScores()
    {
        let context = self.container.viewContext
        let info = ModelStore.bundle.infoDictionary!
        let endpoint = info["SatAPI"] as? String
        let satClient = RestClient(endpoint: endpoint ?? "", context: context, session: URLSession.shared)
        satClient.data().sink(receiveCompletion: { completion in
            switch completion
            {
            case .finished:
                break
            case .failure:
                break
            }
        }, receiveValue: { [weak self] (satScores: [SAT]) in
            var satMap = [String: SAT]()
            for sat in satScores
            {
                satMap[sat.dbn!] = sat
            }

            for school in self?.schools(updateRequest: nil) ?? []
            {
                school.sat = satMap[school.dbn!]
            }

            self?.save(context: context)
        }).store(in: &cancellables)
    }
}
