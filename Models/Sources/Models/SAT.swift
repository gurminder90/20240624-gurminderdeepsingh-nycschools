//
//  SAT+CoreDataClass.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(SAT)
public class SAT: NSManagedObject, Decodable
{
    private enum Keys: String, CodingKey
    {
        case dbn = "dbn"
        case total_students = "num_of_sat_test_takers"
        case reading_score = "sat_critical_reading_avg_score"
        case math_score = "sat_math_avg_score"
        case writing_score = "sat_writing_avg_score"
    }
    
    required convenience public init(from decoder: Decoder) throws
    {
        self.init(insertInto: Self.managedContext(from: decoder))
        
        let values = try decoder.container(keyedBy: Keys.self)
        
        guard let id = try values.decodeIfPresent(String.self, forKey: .dbn) else
        {
            fatalError("DBN should not be empty")
        }
        self.dbn = id
        
        if let totalStudentsValue = try values.decodeIfPresent(String.self, forKey: .total_students)
        {
            if let totalStudents = Int(totalStudentsValue)
            {
                self.total_students = NSNumber(value: totalStudents)
            }
        }
        
        if let readingScoreValue = try values.decodeIfPresent(String.self, forKey: .reading_score)
        {
            if let readingScore = Int(readingScoreValue)
            {
                self.reading_score = NSNumber(value: readingScore)
            }
        }
        
        if let mathScoreValue = try values.decodeIfPresent(String.self, forKey: .math_score)
        {
            if let mathScore = Int(mathScoreValue)
            {
                self.math_score = NSNumber(value: mathScore)
            }
        }
        
        if let writingScoreValue = try values.decodeIfPresent(String.self, forKey: .writing_score)
        {
            if let writingScore = Int(writingScoreValue)
            {
                self.writing_score = NSNumber(value: writingScore)
            }
        }
    }}
