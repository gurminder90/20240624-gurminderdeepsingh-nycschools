//
//  School+CoreDataProperties.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

extension School
{
    @nonobjc public class func fetchRequest() -> NSFetchRequest<School>
    {
        return NSFetchRequest<School>(entityName: "School")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var name: String?
    @NSManaged public var overview: String?
    @NSManaged public var website: String?
    @NSManaged public var phone_number: String?
    @NSManaged public var fax_number: String?
    @NSManaged public var total_students: NSNumber?
    @NSManaged public var graduation: NSNumber?
    @NSManaged public var attendance: NSNumber?
    @NSManaged public var email: String?
    @NSManaged public var borough: Borough?
    @NSManaged public var address: Address?
    @NSManaged public var sat: SAT?
}
