//
//  School+CoreDataClass.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(School)
public class School: NSManagedObject, Decodable
{
    private enum Keys: String, CodingKey
    {
        case dbn = "dbn"
        case name = "school_name"
        case overview = "overview_paragraph"
        case website = "website"
        case email = "school_email"
        case phone_number = "phone_number"
        case fax_number = "fax_number"
        case total_students = "total_students"
        case graduation = "graduation_rate"
        case attendance = "attendance_rate"
        
        case borough = "boro"
    }
    
    required convenience public init(from decoder: Decoder) throws
    {
        let managedContext = Self.managedContext(from: decoder)
        self.init(insertInto: managedContext)
        
        let values = try decoder.container(keyedBy: Keys.self)
        
        guard let id = try values.decodeIfPresent(String.self, forKey: .dbn) else
        {
            fatalError("DBN should not be empty")
        }
        self.dbn = id
        
        guard let name = try values.decodeIfPresent(String.self, forKey: .name) else
        {
            fatalError("Name should not be empty")
        }
        self.name = name
        
        guard let overview = try values.decodeIfPresent(String.self, forKey: .overview) else
        {
            fatalError("Overview should not be empty")
        }
        self.overview = overview
        
        self.website = try values.decodeIfPresent(String.self, forKey: .website)
        self.email = try values.decodeIfPresent(String.self, forKey: .email)
        self.phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
        self.fax_number = try values.decodeIfPresent(String.self, forKey: .fax_number)
    
        if let totalStudentsValue = try values.decodeIfPresent(String.self, forKey: .total_students)
        {
            if let totalStudents = Int(totalStudentsValue)
            {
                self.total_students = NSNumber(value: totalStudents)
            }
        }
        
        if let attendanceValue = try values.decodeIfPresent(String.self, forKey: .attendance)
        {
            if let attendance = Float(attendanceValue)
            {
                self.attendance = NSNumber(value: attendance)
            }
        }
        
        if let graduationValue = try values.decodeIfPresent(String.self, forKey: .graduation)
        {
            if let graduation = Float(graduationValue)
            {
                self.graduation = NSNumber(value: graduation)
            }
        }
        
        self.address = try Address(from: decoder)
        
        let request = NSFetchRequest<Borough>(entityName: NSStringFromClass(Borough.self))
        let boroughName = try values.decodeIfPresent(String.self, forKey: .borough) ?? ""
        request.predicate = NSPredicate(format: "code == %@", boroughName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        
        if let existingBorough = try managedContext.fetch(request).first
        {
            self.borough = existingBorough
        }
        else
        {
            self.borough = try Borough(from: decoder)
        }
    }
}
