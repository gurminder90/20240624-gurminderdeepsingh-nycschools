//
//  Address+CoreDataProperties.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

extension Address
{

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address>
    {
        return NSFetchRequest<Address>(entityName: "Address")
    }

    @NSManaged public var street1: String?
    @NSManaged public var city: String?
    @NSManaged public var zip: String?
    @NSManaged public var latitude: NSNumber?
    @NSManaged public var longitude: NSNumber?
    @NSManaged public var school: School

}
