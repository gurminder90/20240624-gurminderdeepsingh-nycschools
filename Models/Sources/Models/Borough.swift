//
//  Borough+CoreDataClass.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Borough)
public class Borough: NSManagedObject, Decodable
{
    private enum Keys: String, CodingKey
    {
        case name = "borough"
        case code = "boro"
    }
    
    required convenience public init(from decoder: Decoder) throws
    {
        self.init(insertInto: Self.managedContext(from: decoder))
        
        let values = try decoder.container(keyedBy: Keys.self)
        
        let name = try values.decodeIfPresent(String.self, forKey: .name)
        self.name = name?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.code = try values.decodeIfPresent(String.self, forKey: .code)
    }
}
