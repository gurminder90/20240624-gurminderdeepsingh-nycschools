//
//  SAT+CoreDataProperties.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

extension SAT
{
    @nonobjc public class func fetchRequest() -> NSFetchRequest<SAT>
    {
        return NSFetchRequest<SAT>(entityName: "SAT")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var total_students: NSNumber?
    @NSManaged public var reading_score: NSNumber?
    @NSManaged public var math_score: NSNumber?
    @NSManaged public var writing_score: NSNumber?
    @NSManaged public var school: School?
}
