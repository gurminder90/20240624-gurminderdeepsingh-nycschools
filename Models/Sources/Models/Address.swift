//
//  Address+CoreDataClass.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Address)
public class Address: NSManagedObject, Decodable
{
    private enum Keys: String, CodingKey
    {
        case street1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case latitude = "latitude"
        case longitude = "longitude"
    }
    
    required convenience public init(from decoder: Decoder) throws
    {
        self.init(insertInto: Self.managedContext(from: decoder))
        
        let values = try decoder.container(keyedBy: Keys.self)
        
        self.street1 = try values.decodeIfPresent(String.self, forKey: .street1)
        self.city = try values.decodeIfPresent(String.self, forKey: .city)
        self.zip = try values.decodeIfPresent(String.self, forKey: .zip)
        
        if let latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        {
            self.latitude = NSNumber(value: Float(latitude)!)
        }
        
        if let longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        {
            self.longitude = NSNumber(value: Float(longitude)!)
        }
    }
}
