//
//  Borough+CoreDataProperties.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//
//

import Foundation
import CoreData

extension Borough
{

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Borough>
    {
        return NSFetchRequest<Borough>(entityName: "Borough")
    }

    @NSManaged public var name: String?
    @NSManaged public var code: String?
    @NSManaged public var schools: [School]?

}
