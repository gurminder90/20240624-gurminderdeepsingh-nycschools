//
//  NSManagedObject+Init.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import CoreData


extension NSManagedObject
{
    class func managedContext(from decoder: Decoder) -> NSManagedObjectContext
    {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else
        {
            fatalError("Failed to decode " + NSStringFromClass(Self.self))
        }
        
        guard let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext else
        {
            fatalError("Failed to decode " + NSStringFromClass(Self.self))
        }
        
        return managedObjectContext
    }
    
    convenience public init(insertInto context: NSManagedObjectContext)
    {
        guard let entity = NSEntityDescription.entity(forEntityName: NSStringFromClass(Self.self), in: context) else
        {
            fatalError("Failed to decode " + NSStringFromClass(Self.self))
        }
        
        self.init(entity: entity, insertInto: context)
    }
}
