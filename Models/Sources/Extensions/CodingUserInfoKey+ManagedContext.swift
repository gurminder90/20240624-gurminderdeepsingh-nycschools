//
//  CodingUserInfoKey+ManagedContext.swift
//  Models
//
//  Created by Gurminder Deep Singh on 10/5/19.
//  Copyright © 2019 Gurminder. All rights reserved.
//

import Foundation

public extension CodingUserInfoKey
{
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}
